import 'dart:async';

import 'package:habit_tracker/classes/task.dart';
import 'package:habit_tracker/repository/taskState.dart';

abstract class TaskEvents {}

class AddTask extends TaskEvents {
  final Task newTask;

  AddTask(this.newTask);
}

class RemoveTask extends TaskEvents {
  final int index;
  RemoveTask(this.index);
}

class ChangeTask extends TaskEvents {
  final int index;
  final Task changedTask;
  ChangeTask(this.index, this.changedTask);
}

class TaskBloc {
  TaskState state = TaskState.create();

  final _taskStateController = StreamController<TaskState>();

  StreamSink<TaskState> get _inTaskState => _taskStateController.sink;

  Stream<TaskState> get taskState => _taskStateController.stream;

  final _taskEventController = StreamController<TaskEvents>();

  Sink<TaskEvents> get taskEventSink => _taskEventController.sink;

  TaskBloc(){
    _taskEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(TaskEvents event) {
    if (event is AddTask)
      state = addTask(event.newTask);
    else if (event is RemoveTask)
      state = removeTask(event.index);
    else if (event is ChangeTask)
      state = changeTask(event.index, event.changedTask);
    _inTaskState.add(state);
  }

  TaskState addTask(Task newTask) {
    return TaskState(state.tasks..add(newTask));
  }

  TaskState removeTask(int index) {
    return TaskState(state.tasks..removeAt(index));
  }

  TaskState changeTask(int index, Task changedTask) {
    return TaskState(state.tasks
      ..removeAt(index)
      ..insert(index, changedTask));
  }

  void dispose(){
    _taskEventController.close();
    _taskStateController.close();
  }
}
