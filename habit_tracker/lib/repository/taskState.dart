import 'package:habit_tracker/classes/task.dart';

class TaskState{
  final List<Task> tasks;

  TaskState(this.tasks);

  TaskState.create() : this.tasks = List<Task>()..add(Task(name: 'First Task', initialDay: DateTime.now().subtract(Duration(days:5))));

}