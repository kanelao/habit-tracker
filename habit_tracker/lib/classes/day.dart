import 'package:flutter/material.dart';

class Day {
  DateTime time;
  bool _completed;

  Day({@required this.time, bool completed = false}) : _completed = completed;

  void changeCompletion(){
    _completed = !_completed;
  }

  bool getCompletion(){
    return _completed;
  }
}
