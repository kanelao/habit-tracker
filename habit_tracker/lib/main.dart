import 'package:flutter/material.dart';
import 'package:habit_tracker/classes/task.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  ThemeData appTheme() {
    return ThemeData(
      primarySwatch: Colors.blueGrey,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Flutter Demo', theme: appTheme(), home: Test());
  }
}

class Test extends StatefulWidget {
  final Task task = Task.create(name: 'Text');

  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  Widget iconForTaskCompletionOfTheGivenDay(Task task, int index) {
    return Container(
        width: 50.0,
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: GestureDetector(
            child: icon(task, index),
            onTap: () {
              setState(() {
                task.changeCompletionOfTheGivenDay(index);
              });
            }));
  }

  Icon icon(Task task, int index) {
    return Icon(
        task.wasTaskCompletedOnTheGivenDay(index) ? Icons.check : Icons.clear);
  }

  Widget columnHeader(String day, String number) {
    return Container(
      width: 50.0,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: <Widget>[
          Text(day),
          Text(number),
        ],
      ),
    );
  }

  AppBar appBar() {
    return AppBar(
      title: Text(
        'Habits',
        style: TextStyle(color: Colors.white),
      ),
      actions: <Widget>[
        IconButton(icon: Icon(Icons.add), onPressed: () {}),
        IconButton(icon: Icon(Icons.filter_list), onPressed: () {}),
        IconButton(icon: Icon(Icons.more_vert), onPressed: () {})
      ],
    );
  }

  List<Widget> taskBuilder(Task task) {
    List<Widget> taskCompletion = List<Widget>();
    for (int i = 0; i < 5; i++) {
      taskCompletion.add(iconForTaskCompletionOfTheGivenDay(task, i));
    }
    return taskCompletion;
  }

  List<Widget> dayBuilder() {
    List<Widget> days = List<Widget>();
    for (int i = 0; i < 5; i++) {
      days.add(columnHeader(weekday(widget.task.days[4 - i].time.weekday),
          widget.task.days[4 - i].time.day.toString()));
    }
    return days;
  }

  String weekday(int day) {
    switch (day) {
      case 1:
        return 'MON';
        break;
      case 2:
        return 'TUE';
        break;
      case 3:
        return 'WED';
        break;
      case 4:
        return 'THU';
        break;
      case 5:
        return 'FRI';
        break;
      case 6:
        return 'SAT';
        break;
      case 7:
        return 'SUN';
        break;
      default:
        return 'ERR';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: Column(
        children: <Widget>[
          Container(
            color: Colors.grey,
            padding: EdgeInsets.all(14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: dayBuilder(),
            ),
          ),
          Container(
            padding: EdgeInsets.all(14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(Icons.add_circle_outline),
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        child: Text(widget.task.name)),
                  ],
                ),
                Row(
                  children: taskBuilder(widget.task),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
