import 'package:flutter/material.dart';
import 'package:habit_tracker/classes/day.dart';

class Task {
  String name;
  List<Day> days;

  Task({
    @required this.name,
    @required DateTime initialDay,
  }) {
    days = List<Day>();
    DateTime today = DateTime.now();
    for (var day = initialDay;
        day.compareTo(today) != 0;
        day.add(Duration(days: 1))) {
      days.add(Day(time: day));
    }
  }

  bool wasTaskCompletedOnTheGivenDay(int index) {
    return days[index].getCompletion();
  }

  // int _findDayIndex(DateTime dayToFind){
  //   days.firstWhere((Day day) => day.time)
  //   return 0;
  // }

  void changeCompletionOfTheGivenDay(int index) {
    days[index].changeCompletion();
  }

  Task.create({@required this.name}) {
    days = List<Day>();
    for (int i = 0; i < 5; i++) {
      days.add(Day(time: DateTime.now().subtract(Duration(days: i))));
    }
  }
}
