// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';
import 'package:habit_tracker/classes/task.dart';

void main() {

  test('Date Finder Works', () {
    Task task = Task(initialDay: DateTime(2019), name: 'test', );
    var numberOfDays = DateTime.now().difference(DateTime(2019)).inDays;
    assert(1 == 0);
    assert(task.days.length == numberOfDays);
    expect(task.days.length, numberOfDays);
    print(numberOfDays);
  });
}
