import 'package:flutter/material.dart';
import 'package:habit_tracker/bloc/task_bloc.dart';
import 'package:habit_tracker/classes/task.dart';
import 'package:habit_tracker/repository/taskState.dart';

class TaskPage extends StatelessWidget {
  final TaskBloc _taskStream = TaskBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: StreamBuilder(
        initialData: _taskStream.state,
        stream: _taskStream.taskState,
        builder: (BuildContext context, AsyncSnapshot<TaskState> snapshot) {
          return ListView(
            children: <Widget>[
              tableHeader(),
              taskEntry(snapshot.data.tasks[0]),
            ],
          );
        },
      ),
    );
  }

  AppBar appBar() {
    return AppBar(
      title: Text(
        'Habits',
        style: TextStyle(color: Colors.white),
      ),
      actions: actions(),
    );
  }

  List<Widget> actions() {
    return [
      IconButton(icon: Icon(Icons.add), onPressed: () {}),
      IconButton(icon: Icon(Icons.filter_list), onPressed: () {}),
      IconButton(icon: Icon(Icons.more_vert), onPressed: () {})
    ];
  }

  Widget tableHeader() {
    return Container(
      color: Colors.grey,
      padding: EdgeInsets.all(14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: columnHeadersBuilder(DateTime.now()),
      ),
    );
  }

  List<Widget> columnHeadersBuilder(DateTime date) {
    List<Widget> days = List<Widget>();
    for (int i = 0; i < 5; i++) {
      days.add(columnHeader(weekday(date.weekday), date.day.toString()));
    }
    return days;
  }

  Widget columnHeader(String day, String number) {
    return Container(
      width: 50.0,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: <Widget>[
          Text(day),
          Text(number),
        ],
      ),
    );
  }

  Widget taskEntry(Task task) {
    return Container(
      padding: EdgeInsets.all(14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.add_circle_outline),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  child: Text(task.name)),
            ],
          ),
          Row(
            children: completionEntriesForGivenTask(task),
          ),
        ],
      ),
    );
  }

  List<Widget> completionEntriesForGivenTask(Task task) {
    List<Widget> taskCompletion = List<Widget>();
    for (int i = 0; i < 5; i++) {
      taskCompletion.add(iconForTaskCompletionOfTheGivenDay(task, i));
    }
    return taskCompletion;
  }

  Widget iconForTaskCompletionOfTheGivenDay(Task task, int index) {
    return Container(
        width: 50.0,
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: GestureDetector(
            child: completionIcon(task, index),
            onTap: () {
              task.changeCompletionOfTheGivenDay(index);
            }));
  }

  Icon completionIcon(Task task, int index) {
    return Icon(
        task.wasTaskCompletedOnTheGivenDay(index) ? Icons.check : Icons.clear);
  }

  static String weekday(int day) {
    switch (day) {
      case 1:
        return 'MON';
        break;
      case 2:
        return 'TUE';
        break;
      case 3:
        return 'WED';
        break;
      case 4:
        return 'THU';
        break;
      case 5:
        return 'FRI';
        break;
      case 6:
        return 'SAT';
        break;
      case 7:
        return 'SUN';
        break;
      default:
        return 'ERR';
    }
  }
}
